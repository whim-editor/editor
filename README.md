# Whim Editor

Whim is an attempt to create the best possible editor for the web, focusing on a scalable, flexible
approach to editing documents. Whim takes lessons learned from editors of the past, including Vim,
Emacs, and the myriad IDEs, and integrates the magic of the modern web. We believe our editors can
be powerful, extensible, efficient, and beautiful at the same time. We want to shift the paradigm.

More information coming soon.

## Differences from Atom

- Intends to be composable from the ground up.
- Runs in a web browser
- Doesn't follow ancient IDE interface paradigms

## Source Code

Multiple Modules:

- `core`: reusable components at the heart of whim
  - `text`: modules for manipulating text
    - text object
    - context
    - ranges (e.g. motions)
    - transformers (e.g. operators)
  - syntax
- `editor`: the HTML5 editor widget
  - `buffer`
  - `cursor`
  - highlighting
  - keybindings
- `ide`: a robust editor
  - `resources`: the entities to which buffers map (e.g. files and repos)
  - keybindings (trickle down to editor)
  - plugin manager
  - buffer manager
  - buffer switching directive
  - command line


## Organization

Below is an overview of how this code is organized as well as how it is planned to be organized.

- `src/`
  - `app/`
  - `common/`
    - [ ] `functions/` - The Whim function manager; functions are defined here.
    - [ ] `operators/` - The Whim operator manager; operators are defined here.
    - [ ] `motions/` - The Whim motion manager; motions are defined here.
    - [ ] `buffers/`
      - [ ] `editor/`
        - [ ] `cursor/`
        - [ ] `hidden-input/`
        - [ ] `editor.coffee` - The main editor directive. Operates on a buffer.
      - [ ] `manager/`
      - [ ] `switcher/` - A directive for allowing the user to switch between open buffers.
      - [ ] `Buffer.coffee` - A class that represents a single buffer.
    - [ ] `syntax/`
    - [ ] `resources/` - A manager to handle the external items to which buffers map (e.g.  files).
    - [ ] `input/`
      - [ ] `modal-model` - A directive to handle modal input.
      - [ ] `keys` - A key handler.
  - `plugins/` - The core plugins included with Whim by default.
    - [ ] `tree-view/` - A tool to show the available resources in a hierarchical tree.
    - [ ] `function-directory/` - A tool to navigate available functions.
    - [ ] `command-bar/` - A directive that displays the enabled plugin tools.
    - [ ] `ctrl-p/`
    - [ ] `file-save/`
    - [ ] `file-close/`
  - `variables.scss`
  - `main.scss`

